Payroll does not recalculate your retirement account contributions automatically, so this program will help you do that on your own.

Instructions:  
1. Clone repo, get a copy of your latest paystub.
2. Compile the classes: `javac calculator/*.java`
3. Run the program: `java calculator.Calculator` and follow the prompts

Example:

```
$ java calculator.Calculator 
Please enter the total number of pay periods: 
24
Please enter the number of the most recent pay period for which you were paid: 
14 
Please enter the EmployEE contribution basis: 
2000
Please enter the EmployER contribution basis: 
2000
Please enter the desired contribution goal: 
19500
Please enter the previous contribution you made towards the goal: 
10000
You must contribute $870.00 or 43.50 percent of your gross pay every remaining pay period to reach your goal of $19,500.00
```
