package calculator;
import java.util.Scanner;

/*
 * Blueprint for employee's account
 */
public class Goal {
  double goal, previousContribution;
  final double EMPLOYER_MATCH_PERCENTAGE = 0.04; // 4%
  double myRemainingContribution, employerRemainingContribution;
  double myFinalContributionAmount;

  public double GetGoal() {
    return goal;
  }
  public void SetGoal() {
    Scanner in = new Scanner(System.in);
    System.out.println("Please enter the desired contribution goal: ");
    double input = in.nextDouble();
    if (input < 0) {
      System.out.print("Error: You must enter a positive number!");
    }
    else {
      goal = input;
    }
  }
  
  public double GetPreviousContribution() {
    return previousContribution;
  }
  public void SetPreviousContribution() {
    Scanner in = new Scanner(System.in);
    System.out.println("Please enter the previous contribution you made towards the goal: ");
    double input = in.nextDouble();
    if (input < 0) {
      System.out.print("Error: You must enter a positive number!");
    }
    else {
      previousContribution = input;
    }
  }

  public void CalculateRemainingContributions(double myBase, double empBase, int remainderPPs) {
    employerRemainingContribution = (empBase * EMPLOYER_MATCH_PERCENTAGE) * remainderPPs;
    myRemainingContribution = (goal - previousContribution) - employerRemainingContribution;
    myFinalContributionAmount = myRemainingContribution / remainderPPs;
  }
  public double GetMyFinalContributionAmount() {
    return myFinalContributionAmount;
  }
}