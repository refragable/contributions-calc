package calculator;
import java.util.Scanner;

/*
 * Blueprint for the employee's paystub
 */
public class Income {
  int totalPayPeriods, lastPayPeriod, remainingPayperiods;

  double myContributionBase, employerContributionBase;

  public int GetTotalPayPeriods() {
    return totalPayPeriods;
  }
  public void SetTotalPayPeriods() {
    Scanner in = new Scanner(System.in);
    System.out.println("Please enter the total number of pay periods: ");
    int input = in.nextInt();
    if (input < 0) {
      System.out.print("Error: You must enter a positive number!");
    }
    else {
      totalPayPeriods = input;
      //System.out.println("You entered: " + totalPayPeriods);
    }
  }

  public int GetLastPayPeriod() {
    return lastPayPeriod;
  }
  public void SetLastPayperiod() {
    Scanner in = new Scanner(System.in);
    System.out.println("Please enter the number of the most recent pay period for which you were paid: ");
    int input = in.nextInt();
    if (input < 0) {
      System.out.print("Error: You must enter a positive number!");
    }
    else {
      lastPayPeriod = input;
      //System.out.println("You entered: " + lastPayPeriod);
    }
  }

  public int GetRemainingPayPeriods() {
    return remainingPayperiods;
  }
  public void SetRemainingPayPeriods() {
    remainingPayperiods = GetTotalPayPeriods() - GetLastPayPeriod();
    //System.out.println("Calculated: " + remainingPayperiods + " remaining pay periods.");
  }

  public double GetMyContributionBase() {
    return myContributionBase;
  }
  public void SetMyContributionBase() {
    Scanner in = new Scanner(System.in);
    System.out.println("Please enter the EmployEE contribution basis: ");
    double input = in.nextDouble();
    if (input < 0) {
      System.out.print("Error: You must enter a positive number!");
    }
    else {
      myContributionBase = input;
    }
  }

  public double GetEmployerContributionBase() {
    return employerContributionBase;
  }
  public void SetEmployerContributionBase() {
    Scanner in = new Scanner(System.in);
    System.out.println("Please enter the EmployER contribution basis: ");
    double input = in.nextDouble();
    if (input < 0) {
      System.out.print("Error: You must enter a positive number!");
    }
    else {
      employerContributionBase = input;
    }
  }
}