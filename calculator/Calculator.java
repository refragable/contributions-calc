package calculator;

public class Calculator {
  public static void main(String[] args) {
    Income paystub1 = new Income();
    Goal account1 = new Goal();

    paystub1.SetTotalPayPeriods();
    paystub1.SetLastPayperiod();
    paystub1.SetRemainingPayPeriods();
    paystub1.SetMyContributionBase();
    paystub1.SetEmployerContributionBase();
    account1.SetGoal();
    account1.SetPreviousContribution();
    account1.CalculateRemainingContributions(paystub1.GetMyContributionBase(), paystub1.GetEmployerContributionBase(), paystub1.GetRemainingPayPeriods());
    System.out.printf("You must contribute $%,.2f or %,.2f percent of your gross pay every remaining pay period to reach your goal of $%,.2f", account1.GetMyFinalContributionAmount(), account1.GetMyFinalContributionAmount() / paystub1.GetMyContributionBase() * 100, account1.GetGoal());
  }
}